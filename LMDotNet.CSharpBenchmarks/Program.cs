﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Attributes.Columns;
using BenchmarkDotNet.Attributes.Jobs;
using BenchmarkDotNet.Running;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMDotNet.CSharpBenchmarks
{
    [RyuJitX64Job]
    //[AllStatisticsColumn]
    public class Fit
    {
        [Params(LMBackend.NativeLmmin, LMBackend.ManagedLmmin)]
        public LMBackend Solver { get; set; }

        LMSolver solver;
        double[] ts;
        double[] sins;
        double[] initialParams;

        public Fit() {
            solver = new LMSolver(optimizerBackend: Solver);

            // for generating some noise
            var rand = new Random(12345);
            // sample points (here: equidistant)
            ts = Enumerable.Range(0, 10000).Select(i => i * 0.01).ToArray();
            // signal: "measured" values at sample points
            sins = Enumerable.Range(0, 10000)
                .Select(i => 2.0 * Math.Sin(3.0 * ts[i] + 0.25) + 2.23 + rand.NextDouble() * 0.02)
                .ToArray();

            initialParams = new[] { 1.0, 1.0, 1.0, 1.0 };
        }

        [Benchmark]
        public OptimizationResult FitNoisySine() {            
            // FitCurve evaluates the model p[0] * Math.Sin(p[1] * t + p[2]) + p[3]
            // for each t and computes the residual based on the corresponding y value
            return solver.FitCurve((t, p) => p[0] * Math.Sin(p[1] * t + p[2]) + p[3], initialParams, ts, sins);            
        }
    }

    class Program
    {
        static void Main(string[] args) {
            var summary = BenchmarkRunner.Run<Fit>();                        
        }
    }
}
